import os
from setuptools import setup, find_packages

ROOT_DIR = os.path.dirname(__file__)
with open(os.path.join(ROOT_DIR, "README.md")) as readme:
    README = readme.read()


def _lines_from_file(filename):
    with open(os.path.join(ROOT_DIR, filename)) as f:
        lines = f.readlines()
    return lines


def get_extra_requires():
    extras = {
        # テスト用のパッケージ
        "test": ["pytest"]
    }
    return extras


setup(
    name="networks",
    version="0.8.2",
    author="Akinori Ito",
    author_email="ito@cssc.co.jp",
    url="https://gitlab.com/yv-data/networks",
    packages=find_packages(),
    package_data={"networks": ["yml/*.yml"]},
    description="",
    long_description=README,
    install_requires=_lines_from_file("docker/requirements.txt"),
    extras_require=get_extra_requires(),
)
