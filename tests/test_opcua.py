from opcua import ua
from opcua.client.ua_client import UaClient
from opcua.common.node import Node
from networks.protocol.yv_opcua import YvOpcUa

# OPCサーバー
conf = {
    "host": "192.168.102.2",
    "port": "64121",
    "server": "GeCssOpcUaServer",
    "security_policy": "Basic256",
    "security_mode": "SignAndEncrypt",
    "certificate": "/working/credentials/Pure Python Client [F344D7BA7BCDD8B15C8EFC953278F3DE390B3B8C].der",
    "private_key": "/working/credentials/Pure Python Client [F344D7BA7BCDD8B15C8EFC953278F3DE390B3B8C].pem",
    "item": ["ns=2;s=WTG-001.V_WIN"]
}

opc = YvOpcUa(conf)


def test_connect():
    try:
        root = opc.client.get_root_node()
        assert root == Node(
            UaClient(4), ua.TwoByteNodeId(ua.ObjectIds.RootFolder))
        assert len(root.get_children()) > 2

        data_list = opc.send()
        assert type(data_list[0][0]) == float
    finally:
        opc.client.disconnect()
