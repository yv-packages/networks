import os
from setuptools import setup, find_packages

ROOT_DIR = os.path.dirname(__file__)

def _lines_from_file(filename):
    with open(os.path.join(ROOT_DIR, filename)) as f:
        lines = f.readlines()
    return lines

print(_lines_from_file('../docker/requirements.txt'))