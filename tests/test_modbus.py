import pytest
import time
from networks.protocol.yv_modbus import YvModbus

# テスト用のModbusサーバーの設定
conf = {"host": "192.168.1.63", "port": 502, "unit_id": 1}
modbus = YvModbus(conf)


def test_contants():
    assert conf["host"] == modbus.host
    assert conf["port"] == modbus.port
    assert conf["unit_id"] == modbus.unit_id


@pytest.mark.skip(reason="To avoid accessing the server")
def test_send():
    # Write multiple coils (function 0x0F)
    data_0F = [1, 1, 0, 1]
    assert data_0F == modbus.send(function_code="0x0F", addr=0, data=data_0F)
    time.sleep(1)
    # Read coils (function 0x01)
    assert data_0F == modbus.send(function_code="0x01", addr=0, d_length=4)
    time.sleep(1)

    # Write multiple registers (function 0x10)
    data_10 = [1, 64, 128, 255]
    assert data_10 == modbus.send(function_code="0x10", addr=0, data=data_10)
    time.sleep(1)

    # Read holding register (function 0x03)
    data_04 = [1, 64, 128, 255]
    assert data_04 == modbus.send(function_code="0x03", addr=0, d_length=4)
    time.sleep(1)


true_conf_lists = [
    (
        "mukawa",
        "rio00-01",
        "read",
        [
            {
                "function_code": "0x04",
                "adress": 16,
                "data_length": [2, 2],
                "name": ["subst_energy_receive", "subst_energy_transmission"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "mukawa",
        "rio00-02",
        "read",
        [
            {
                "function_code": "0x02",
                "adress": 2,
                "data_length": [1, 1, 1, 1, 1, 1, 1, 1, 1],
                "name": [
                    "vd",
                    "esr1",
                    "n89r",
                    "esr2",
                    "n52r",
                    "esr3",
                    "n89r_il",
                    "n52f",
                    "n89l",
                ],
                "type": [
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                ],
                "endian": [
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                ],
            }
        ],
    ),
    (
        "mukawa",
        "rio00-03",
        "read",
        [
            {
                "function_code": "0x02",
                "adress": 0,
                "data_length": [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                "name": [
                    "n30vd",
                    "n63gisl",
                    "n51r",
                    "n51gr",
                    "n27r",
                    "n59r",
                    "n95hr",
                    "n95lr",
                    "n64r",
                    "n80gis",
                    "n80k",
                    "n30rr",
                    "n30cdt",
                    "n30bat",
                    "n87t",
                    "n63p",
                ],
                "type": [
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                ],
                "endian": [
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                ],
            }
        ],
    ),
    (
        "mukawa",
        "rio00-04",
        "read",
        [
            {
                "function_code": "0x02",
                "adress": 0,
                "data_length": [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                "name": [
                    "n96p",
                    "n26t",
                    "n33q",
                    "n27b",
                    "n64b",
                    "n51f",
                    "n51gf",
                    "n30pf",
                    "n30l",
                    "n26ts",
                    "n51gt",
                    "n80mc",
                    "spf",
                ],
                "type": [
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                ],
                "endian": [
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                ],
            }
        ],
    ),
    (
        "mukawa",
        "rio01-03",
        "read",
        [
            {
                "function_code": "0x03",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment01", "subst_acpower_curtailment02"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "mukawa",
        "rio02-02",
        "read",
        [
            {
                "function_code": "0x03",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment03", "subst_acpower_curtailment04"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "mukawa",
        "rio03-03",
        "read",
        [
            {
                "function_code": "0x03",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment05", "subst_acpower_curtailment06"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "mukawa",
        "rio04-02",
        "read",
        [
            {
                "function_code": "0x03",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment07", "subst_acpower_curtailment08"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "mukawa",
        "rio05-03",
        "read",
        [
            {
                "function_code": "0x03",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment09", "subst_acpower_curtailment10"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "mukawa",
        "rio06-03",
        "read",
        [
            {
                "function_code": "0x03",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment11", "subst_acpower_curtailment12"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "mukawa",
        "rio07-02",
        "read",
        [
            {
                "function_code": "0x03",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment13", "subst_acpower_curtailment14"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "mukawa",
        "rio08-03",
        "read",
        [
            {
                "function_code": "0x03",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment15", "subst_acpower_curtailment16"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "mukawa",
        "rio09-03",
        "read",
        [
            {
                "function_code": "0x03",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment17", "subst_acpower_curtailment18"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "mukawa",
        "rio10-03",
        "read",
        [
            {
                "function_code": "0x03",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment19", "subst_acpower_curtailment20"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "mukawa",
        "rio11-02",
        "read",
        [
            {
                "function_code": "0x03",
                "adress": 1024,
                "data_length": [1],
                "name": ["subst_acpower_curtailment21"],
                "type": ["uint"],
                "endian": ["big"],
            }
        ],
    ),
    (
        "mukawa",
        "rio12-03",
        "read",
        [
            {
                "function_code": "0x03",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment22", "subst_acpower_curtailment23"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "mukawa",
        "rio13-03",
        "read",
        [
            {
                "function_code": "0x03",
                "adress": 1024,
                "data_length": [1],
                "name": ["subst_acpower_curtailment24"],
                "type": ["uint"],
                "endian": ["big"],
            }
        ],
    ),
    (
        "mukawa",
        "rio01-03",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment01", "subst_acpower_curtailment02"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "mukawa",
        "rio02-02",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment03", "subst_acpower_curtailment04"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "mukawa",
        "rio03-03",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment05", "subst_acpower_curtailment06"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "mukawa",
        "rio04-02",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment07", "subst_acpower_curtailment08"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "mukawa",
        "rio05-03",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment09", "subst_acpower_curtailment10"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "mukawa",
        "rio06-03",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment11", "subst_acpower_curtailment12"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "mukawa",
        "rio07-02",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment13", "subst_acpower_curtailment14"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "mukawa",
        "rio08-03",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment15", "subst_acpower_curtailment16"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "mukawa",
        "rio09-03",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment17", "subst_acpower_curtailment18"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "mukawa",
        "rio10-03",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment19", "subst_acpower_curtailment20"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "mukawa",
        "rio11-02",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1],
                "name": ["subst_acpower_curtailment21"],
                "type": ["uint"],
                "endian": ["big"],
            }
        ],
    ),
    (
        "mukawa",
        "rio12-03",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment22", "subst_acpower_curtailment23"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "mukawa",
        "rio13-03",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1],
                "name": ["subst_acpower_curtailment24"],
                "type": ["uint"],
                "endian": ["big"],
            }
        ],
    ),
    (
        "atsuma",
        "rio00-01",
        "read",
        [
            {
                "function_code": "0x02",
                "adress": 0,
                "data_length": [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                "name": [
                    "vd",
                    "esr1",
                    "n89r",
                    "n52r",
                    "esr2",
                    "esr3",
                    "n89r_il",
                    "n51r",
                    "n51gr",
                    "n27r",
                    "n59r",
                    "n95hr",
                    "n95lr",
                    "n64r",
                    "n30rr",
                    "n80gis",
                ],
                "type": [
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                ],
                "endian": [
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                ],
            }
        ],
    ),
    (
        "atsuma",
        "rio00-02",
        "read",
        [
            {
                "function_code": "0x02",
                "adress": 0,
                "data_length": [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                "name": [
                    "n80k",
                    "n30vd",
                    "n63gisl",
                    "n30mccbb1",
                    "n80mc",
                    "n30cdt",
                    "n87t",
                    "n63p",
                    "n96p",
                    "n26t",
                    "n33q",
                    "n27b",
                    "n64b",
                    "n52f",
                    "n51f",
                    "n51gf",
                ],
                "type": [
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                    "bool",
                ],
                "endian": [
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                    "big",
                ],
            }
        ],
    ),
    (
        "atsuma",
        "rio00-03",
        "read",
        [
            {
                "function_code": "0x02",
                "adress": 0,
                "data_length": [1, 1, 1, 1, 1],
                "name": ["n89l", "n30pf", "n26ts", "n51gt", "n30l"],
                "type": ["bool", "bool", "bool", "bool", "bool"],
                "endian": ["big", "big", "big", "big", "big"],
            }
        ],
    ),
    (
        "atsuma",
        "rio01-03",
        "read",
        [
            {
                "function_code": "0x03",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment01", "subst_acpower_curtailment02"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "atsuma",
        "rio02-03",
        "read",
        [
            {
                "function_code": "0x03",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment03", "subst_acpower_curtailment04"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "atsuma",
        "rio03-02",
        "read",
        [
            {
                "function_code": "0x03",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment05", "subst_acpower_curtailment06"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "atsuma",
        "rio04-02",
        "read",
        [
            {
                "function_code": "0x03",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment07", "subst_acpower_curtailment08"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "atsuma",
        "rio05-03",
        "read",
        [
            {
                "function_code": "0x03",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment09", "subst_acpower_curtailment10"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "atsuma",
        "rio06-02",
        "read",
        [
            {
                "function_code": "0x03",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment11", "subst_acpower_curtailment12"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "atsuma",
        "rio07-02",
        "read",
        [
            {
                "function_code": "0x03",
                "adress": 1024,
                "data_length": [1],
                "name": ["subst_acpower_curtailment13"],
                "type": ["uint"],
                "endian": ["big"],
            }
        ],
    ),
    (
        "atsuma",
        "rio08-02",
        "read",
        [
            {
                "function_code": "0x03",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment14", "subst_acpower_curtailment15"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "atsuma",
        "rio09-02",
        "read",
        [
            {
                "function_code": "0x03",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment16", "subst_acpower_curtailment17"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "atsuma",
        "rio10-03",
        "read",
        [
            {
                "function_code": "0x03",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment18", "subst_acpower_curtailment19"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "atsuma",
        "rio11-02",
        "read",
        [
            {
                "function_code": "0x03",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment20", "subst_acpower_curtailment21"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "atsuma",
        "rio12-02",
        "read",
        [
            {
                "function_code": "0x03",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment22", "subst_acpower_curtailment23"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "atsuma",
        "rio13-02",
        "read",
        [
            {
                "function_code": "0x03",
                "adress": 1024,
                "data_length": [1],
                "name": ["subst_acpower_curtailment24"],
                "type": ["uint"],
                "endian": ["big"],
            }
        ],
    ),
    (
        "atsuma",
        "rio01-03",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment01", "subst_acpower_curtailment02"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "atsuma",
        "rio02-03",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment03", "subst_acpower_curtailment04"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "atsuma",
        "rio03-02",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment05", "subst_acpower_curtailment06"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "atsuma",
        "rio04-02",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment07", "subst_acpower_curtailment08"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "atsuma",
        "rio05-03",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment09", "subst_acpower_curtailment10"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "atsuma",
        "rio06-02",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment11", "subst_acpower_curtailment12"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "atsuma",
        "rio07-02",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1],
                "name": ["subst_acpower_curtailment13"],
                "type": ["uint"],
                "endian": ["big"],
            }
        ],
    ),
    (
        "atsuma",
        "rio08-02",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment14", "subst_acpower_curtailment15"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "atsuma",
        "rio09-02",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment16", "subst_acpower_curtailment17"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "atsuma",
        "rio10-03",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment18", "subst_acpower_curtailment19"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "atsuma",
        "rio11-02",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment20", "subst_acpower_curtailment21"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "atsuma",
        "rio12-02",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment22", "subst_acpower_curtailment23"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "atsuma",
        "rio13-02",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1],
                "name": ["subst_acpower_curtailment24"],
                "type": ["uint"],
                "endian": ["big"],
            }
        ],
    ),
    (
        "kuroiso",
        "rio01-01",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment01", "subst_acpower_curtailment02"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "akagi",
        "rio01-01",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 1024,
                "data_length": [1, 1],
                "name": ["subst_acpower_curtailment01", "subst_acpower_curtailment02"],
                "type": ["uint", "uint"],
                "endian": ["big", "big"],
            }
        ],
    ),
    (
        "kamagayama",
        "pcs01",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 62242,
                "data_length": [2],
                "name": ["subst_acpower_curtailment01"],
                "type": ["float"],
                "endian": ["little"],
            }
        ],
    ),
    (
        "ajigasawa",
        "wcs",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 40001,
                "data_length": [1],
                "name": ["subst_acpower_curtailment"],
                "type": ["uint"],
                "endian": ["big"],
            }
        ],
    ),
    (
        "ishikariB",
        "rtu_curtailment",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 610,
                "data_length": [1],
                "name": ["subst_acpower_curtailment"],
                "type": ["uint"],
                "endian": ["big"],
            }
        ],
    ),
    (
        "ishikariB",
        "rtu_counter",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 600,
                "data_length": [1],
                "name": ["subst_acpower_counter"],
                "type": ["uint"],
                "endian": ["big"],
            }
        ],
    ),
    (
        "innai",
        "fcu_curtailment",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 6,
                "data_length": [2],
                "name": ["subst_acpower_curtailment"],
                "type": ["uint"],
                "endian": ["big"],
            }
        ],
    ),
    (
        "innai",
        "fcu_counter",
        "write",
        [
            {
                "function_code": "0x10",
                "adress": 0,
                "data_length": [2],
                "name": ["subst_acpower_counter"],
                "type": ["uint"],
                "endian": ["big"],
            }
        ],
    ),
]


@pytest.mark.skip(reason="Confirmed")
@pytest.mark.parametrize("site_id, system_id, m_type, return_tuple", true_conf_lists)
def test_search_conf_list(site_id, system_id, m_type, return_tuple):
    assert return_tuple == modbus.search_conf_list(site_id, system_id, m_type)


def test_create_nested_list():
    response_nested = modbus.create_nested_list(
        data_list=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], data_length=[1, 2, 3, 4]
    )

    assert response_nested == [[0], [1, 2], [3, 4, 5], [6, 7, 8, 9]]


true_read_list = [
    (
        "mukawa",
        "rio00-01",
        "read",
        (
            {
                "subst_energy_receive": 0,
                "subst_energy_transmission": 0,
            }
        ),
    ),
    (
        "mukawa",
        "rio00-02",
        "read",
        (
            {
                "vd": 0,
                "esr1": 0,
                "n89r": 0,
                "esr2": 0,
                "n52r": 0,
                "esr3": 0,
                "n89r_il": 0,
                "n52f": 0,
                "n89l": 0,
            }
        ),
    ),
    (
        "mukawa",
        "rio00-03",
        "read",
        (
            {
                "n30vd": 0,
                "n63gisl": 0,
                "n51r": 0,
                "n51gr": 0,
                "n27r": 0,
                "n59r": 0,
                "n95hr": 0,
                "n95lr": 0,
                "n64r": 0,
                "n80gis": 0,
                "n80k": 0,
                "n30rr": 0,
                "n30cdt": 0,
                "n30bat": 0,
                "n87t": 0,
                "n63p": 0,
            }
        ),
    ),
    (
        "mukawa",
        "rio00-04",
        "read",
        (
            {
                "n96p": 0,
                "n26t": 0,
                "n33q": 0,
                "n27b": 0,
                "n64b": 0,
                "n51f": 0,
                "n51gf": 0,
                "n30pf": 0,
                "n30l": 0,
                "n26ts": 0,
                "n51gt": 0,
                "n80mc": 0,
                "spf": 0,
            }
        ),
    ),
    (
        "mukawa",
        "rio01-03",
        "read",
        ({"subst_acpower_curtailment01": 0, "subst_acpower_curtailment02": 0}),
    ),
    (
        "mukawa",
        "rio02-02",
        "read",
        ({"subst_acpower_curtailment03": 0, "subst_acpower_curtailment04": 0}),
    ),
    (
        "mukawa",
        "rio03-03",
        "read",
        ({"subst_acpower_curtailment05": 0, "subst_acpower_curtailment06": 0}),
    ),
    (
        "mukawa",
        "rio04-02",
        "read",
        ({"subst_acpower_curtailment07": 0, "subst_acpower_curtailment08": 0}),
    ),
    (
        "mukawa",
        "rio05-03",
        "read",
        ({"subst_acpower_curtailment09": 0, "subst_acpower_curtailment10": 0}),
    ),
    (
        "mukawa",
        "rio06-03",
        "read",
        ({"subst_acpower_curtailment11": 0, "subst_acpower_curtailment12": 0}),
    ),
    (
        "mukawa",
        "rio07-02",
        "read",
        ({"subst_acpower_curtailment13": 0, "subst_acpower_curtailment14": 0}),
    ),
    (
        "mukawa",
        "rio08-03",
        "read",
        ({"subst_acpower_curtailment15": 0, "subst_acpower_curtailment16": 0}),
    ),
    (
        "mukawa",
        "rio09-03",
        "read",
        ({"subst_acpower_curtailment17": 0, "subst_acpower_curtailment18": 0}),
    ),
    (
        "mukawa",
        "rio10-03",
        "read",
        ({"subst_acpower_curtailment19": 0, "subst_acpower_curtailment20": 0}),
    ),
    ("mukawa", "rio11-02", "read", ({"subst_acpower_curtailment21": 0})),
    (
        "mukawa",
        "rio12-03",
        "read",
        ({"subst_acpower_curtailment22": 0, "subst_acpower_curtailment23": 0}),
    ),
    ("mukawa", "rio13-03", "read", ({"subst_acpower_curtailment24": 0})),
    (
        "atsuma",
        "rio00-01",
        "read",
        (
            {
                "vd": 0,
                "esr1": 0,
                "n89r": 0,
                "n52r": 0,
                "esr2": 0,
                "esr3": 0,
                "n89r_il": 0,
                "n51r": 0,
                "n51gr": 0,
                "n27r": 0,
                "n59r": 0,
                "n95hr": 0,
                "n95lr": 0,
                "n64r": 0,
                "n30rr": 0,
                "n80gis": 0,
            }
        ),
    ),
    (
        "atsuma",
        "rio00-02",
        "read",
        (
            {
                "n80k": 0,
                "n30vd": 0,
                "n63gisl": 0,
                "n30mccbb1": 0,
                "n80mc": 0,
                "n30cdt": 0,
                "n87t": 0,
                "n63p": 0,
                "n96p": 0,
                "n26t": 0,
                "n33q": 0,
                "n27b": 0,
                "n64b": 0,
                "n52f": 0,
                "n51f": 0,
                "n51gf": 0,
            }
        ),
    ),
    (
        "atsuma",
        "rio00-03",
        "read",
        ({"n89l": 0, "n30pf": 0, "n26ts": 0, "n51gt": 0, "n30l": 0}),
    ),
    (
        "atsuma",
        "rio01-03",
        "read",
        ({"subst_acpower_curtailment01": 0, "subst_acpower_curtailment02": 0}),
    ),
    (
        "atsuma",
        "rio02-03",
        "read",
        ({"subst_acpower_curtailment03": 0, "subst_acpower_curtailment04": 0}),
    ),
    (
        "atsuma",
        "rio03-02",
        "read",
        ({"subst_acpower_curtailment05": 0, "subst_acpower_curtailment06": 0}),
    ),
    (
        "atsuma",
        "rio04-02",
        "read",
        ({"subst_acpower_curtailment07": 0, "subst_acpower_curtailment08": 0}),
    ),
    (
        "atsuma",
        "rio05-03",
        "read",
        ({"subst_acpower_curtailment09": 0, "subst_acpower_curtailment10": 0}),
    ),
    (
        "atsuma",
        "rio06-02",
        "read",
        ({"subst_acpower_curtailment11": 0, "subst_acpower_curtailment12": 0}),
    ),
    ("atsuma", "rio07-02", "read", ({"subst_acpower_curtailment13": 0})),
    (
        "atsuma",
        "rio08-02",
        "read",
        ({"subst_acpower_curtailment14": 0, "subst_acpower_curtailment15": 0}),
    ),
    (
        "atsuma",
        "rio09-02",
        "read",
        ({"subst_acpower_curtailment16": 0, "subst_acpower_curtailment17": 0}),
    ),
    (
        "atsuma",
        "rio10-03",
        "read",
        ({"subst_acpower_curtailment18": 0, "subst_acpower_curtailment19": 0}),
    ),
    (
        "atsuma",
        "rio11-02",
        "read",
        ({"subst_acpower_curtailment20": 0, "subst_acpower_curtailment21": 0}),
    ),
    (
        "atsuma",
        "rio12-02",
        "read",
        ({"subst_acpower_curtailment22": 0, "subst_acpower_curtailment23": 0}),
    ),
    ("atsuma", "rio13-02", "read", ({"subst_acpower_curtailment24": 0})),
    (
        "atsuma",
        "pcs01",
        "read",
        (
            {
                "err_no": 0,
                "riso": 0,
                "mode": 0,
                "error": 0,
                "man_res_stt": 0,
                "cnt_fan_cab1": 0,
                "cnt_fan_cab2": 0,
                "cnt_fan_cab3": 0,
                "ipv": 0,
                "vpv": 0,
                "ppv": 0,
                "pac": 0,
                "vac_l12": 0,
                "vac_l23": 0,
                "vac_l31": 0,
                "iac": 0,
                "iac_l1": 0,
                "iac_l2": 0,
                "iac_l3": 0,
                "fac": 0,
                "qac": 0,
                "sac": 0,
                "pf": 0,
                "vac": 0,
                "tmp_hs": 0,
                "tmp_cab1": 0,
                "tmp_cab3": 0,
                "tmp_exl1": 0,
            }
        ),
    ),
    (
        "atsuma",
        "sm01-01",
        "read",
        (
            {
                "serial_number": 0,
                "ssm_identifier": 0,
                "i_string1": 0,
                "i_string2": 0,
                "i_string3": 0,
                "i_string4": 0,
                "i_string5": 0,
                "i_string6": 0,
                "i_string7": 0,
                "i_string8": 0,
            }
        ),
    ),
]


@pytest.mark.skip(reason="To avoid accessing the server")
@pytest.mark.parametrize("site_id, system_id, m_type, return_read", true_read_list)
def test_read(site_id, system_id, m_type, return_read):
    response = modbus.read(site_id, system_id)
    time.sleep(1)
    assert return_read == response


true_write_list = [
    (
        "mukawa",
        "rio01-03",
        "write",
        [0, 0],
        {"subst_acpower_curtailment01": 0, "subst_acpower_curtailment02": 0},
    ),
    (
        "mukawa",
        "rio02-02",
        "write",
        [0, 0],
        {"subst_acpower_curtailment03": 0, "subst_acpower_curtailment04": 0},
    ),
    (
        "mukawa",
        "rio03-03",
        "write",
        [0, 0],
        {"subst_acpower_curtailment05": 0, "subst_acpower_curtailment06": 0},
    ),
    (
        "mukawa",
        "rio04-02",
        "write",
        [0, 0],
        {"subst_acpower_curtailment07": 0, "subst_acpower_curtailment08": 0},
    ),
    (
        "mukawa",
        "rio05-03",
        "write",
        [0, 0],
        {"subst_acpower_curtailment09": 0, "subst_acpower_curtailment10": 0},
    ),
    (
        "mukawa",
        "rio06-03",
        "write",
        [0, 0],
        {"subst_acpower_curtailment11": 0, "subst_acpower_curtailment12": 0},
    ),
    (
        "mukawa",
        "rio07-02",
        "write",
        [0, 0],
        {"subst_acpower_curtailment13": 0, "subst_acpower_curtailment14": 0},
    ),
    (
        "mukawa",
        "rio08-03",
        "write",
        [0, 0],
        {"subst_acpower_curtailment15": 0, "subst_acpower_curtailment16": 0},
    ),
    (
        "mukawa",
        "rio09-03",
        "write",
        [0, 0],
        {"subst_acpower_curtailment17": 0, "subst_acpower_curtailment18": 0},
    ),
    (
        "mukawa",
        "rio10-03",
        "write",
        [0, 0],
        {"subst_acpower_curtailment19": 0, "subst_acpower_curtailment20": 0},
    ),
    ("mukawa", "rio11-02", "write", [0], {"subst_acpower_curtailment21": 0}),
    (
        "mukawa",
        "rio12-03",
        "write",
        [0, 0],
        {"subst_acpower_curtailment22": 0, "subst_acpower_curtailment23": 0},
    ),
    ("mukawa", "rio13-03", "write", [0], {"subst_acpower_curtailment24": 0}),
    (
        "atsuma",
        "rio01-03",
        "write",
        [0, 0],
        {"subst_acpower_curtailment01": 0, "subst_acpower_curtailment02": 0},
    ),
    (
        "atsuma",
        "rio02-03",
        "write",
        [0, 0],
        {"subst_acpower_curtailment03": 0, "subst_acpower_curtailment04": 0},
    ),
    (
        "atsuma",
        "rio03-02",
        "write",
        [0, 0],
        {"subst_acpower_curtailment05": 0, "subst_acpower_curtailment06": 0},
    ),
    (
        "atsuma",
        "rio04-02",
        "write",
        [0, 0],
        {"subst_acpower_curtailment07": 0, "subst_acpower_curtailment08": 0},
    ),
    (
        "atsuma",
        "rio05-03",
        "write",
        [0, 0],
        {"subst_acpower_curtailment09": 0, "subst_acpower_curtailment10": 0},
    ),
    (
        "atsuma",
        "rio06-02",
        "write",
        [0, 0],
        {"subst_acpower_curtailment11": 0, "subst_acpower_curtailment12": 0},
    ),
    ("atsuma", "rio07-02", "write", [0], {"subst_acpower_curtailment13": 0}),
    (
        "atsuma",
        "rio08-02",
        "write",
        [0, 0],
        {"subst_acpower_curtailment14": 0, "subst_acpower_curtailment15": 0},
    ),
    (
        "atsuma",
        "rio09-02",
        "write",
        [0, 0],
        {"subst_acpower_curtailment16": 0, "subst_acpower_curtailment17": 0},
    ),
    (
        "atsuma",
        "rio10-03",
        "write",
        [0, 0],
        {"subst_acpower_curtailment18": 0, "subst_acpower_curtailment19": 0},
    ),
    (
        "atsuma",
        "rio11-02",
        "write",
        [0, 0],
        {"subst_acpower_curtailment20": 0, "subst_acpower_curtailment21": 0},
    ),
    (
        "atsuma",
        "rio12-02",
        "write",
        [0, 0],
        {"subst_acpower_curtailment22": 0, "subst_acpower_curtailment23": 0},
    ),
    ("atsuma", "rio13-02", "write", [0], {"subst_acpower_curtailment24": 0}),
    (
        "kuroiso",
        "rio01-01",
        "write",
        [100, 100],
        {"subst_acpower_curtailment01": 100, "subst_acpower_curtailment02": 100},
    ),
    (
        "akagi",
        "rio01-01",
        "write",
        [50, 50],
        {"subst_acpower_curtailment01": 50, "subst_acpower_curtailment02": 50},
    ),
    (
        "kamagayama",
        "pcs01",
        "write",
        [88.2882],
        {"subst_acpower_curtailment01": 88.28820037841797},
    ),
    (
        "ajigasawa",
        "wcs",
        "write",
        [100],
        {"subst_acpower_curtailment": 100},
    ),
    (
        "ishikariB",
        "rtu_curtailment",
        "write",
        [100],
        {"subst_acpower_curtailment": 100},
    ),
    (
        "ishikariB",
        "rtu_counter",
        "write",
        [1],
        {"subst_acpower_counter": 1},
    ),
    (
        "innai",
        "fcu_curtailment",
        "write",
        [1990],
        {"subst_acpower_curtailment": 1990},
    ),
    (
        "innai",
        "fcu_counter",
        "write",
        [1],
        {"subst_acpower_counter": 1},
    ),
]


# @pytest.mark.skip(reason="To avoid accessing the server")
@pytest.mark.parametrize(
    "site_id, system_id, m_type, data, true_write_response", true_write_list
)
def test_write(site_id, system_id, m_type, data, true_write_response):
    response = modbus.write(site_id, system_id, data)
    time.sleep(1)
    assert true_write_response == response


create_nested_para = [([100], [1], [[100]]), ([100], [1, 1], [[100], [100]])]


@pytest.mark.skip(reason="To avoid accessing the server")
@pytest.mark.parametrize("data, d_lengths, create_nested_result", create_nested_para)
def test_create_nested_list(data, d_lengths, create_nested_result):
    response_nested = modbus.create_nested_list(
        data_list=data * sum(d_lengths), data_length=d_lengths
    )

    assert response_nested == create_nested_result


convert_float_to_list_para = [
    (88.2882, "little", [37775, 17072]),
    (88.2882, "big", [17072, 37775]),
    (88.5885, "little", [11600, 17073]),
    (88.5885, "big", [17073, 11600]),
    (0, "little", [0, 0]),
    (0, "big", [0, 0]),
]

# 147, 143, 66, 176
# 93 8F 42 B0
# 37775 17072

# 45, 80, 66, 177
# 2D 50 42 B1
# 11600 17073


@pytest.mark.skip(reason="To avoid accessing the server")
@pytest.mark.parametrize("value, endian, true_sresponse", convert_float_to_list_para)
def test_convert_float_to_list(value, endian, true_sresponse):
    response = modbus.convert_float_to_list(value, endian)
    assert response == true_sresponse


convert_list_to_float_para = [
    ([37775, 17072], "little", 88.28820037841797),
    ([17072, 37775], "big", 88.28820037841797),
    ([11600, 17073], "little", 88.5885009765625),
    ([17073, 11600], "big", 88.5885009765625),
    ([0, 0], "little", 0),
    ([0, 0], "big", 0),
]


@pytest.mark.skip(reason="To avoid accessing the server")
@pytest.mark.parametrize(
    "data_list, endian, true_sresponse", convert_list_to_float_para
)
def test_convert_list_to_float(data_list, endian, true_sresponse):
    response = modbus.convert_list_to_float(data_list, endian)
    assert response == true_sresponse


convert_to_int_rounded_list = [
    (88.2882, "float", 88.2882),
    (88.2882, "int", 88),
    (88.2882, "uint", 88),
    (0, "float", 0.0),
]


@pytest.mark.skip(reason="To avoid accessing the server")
@pytest.mark.parametrize("value, target_type, result", convert_to_int_rounded_list)
def test_convert_to_int_rounded(value, target_type, result):
    assert modbus.convert_to_int_rounded(value, target_type) == result


convert_value_to_para = [
    (1547.5, 1, "uint", "big", [1548]),
    (1000, 2, "uint", "big", [3, 232]),
    (88.28820037841797, 2, "float", "little", [37775, 17072]),
    (0, 2, "float", "little", [0, 0]),
]


@pytest.mark.skip(reason="To avoid accessing the server")
@pytest.mark.parametrize(
    "value, d_length, type_var, endian, result_true", convert_value_to_para
)
def test_convert_value_to_list(value, d_length, type_var, endian, result_true):
    result_list = modbus.convert_value_to_list(value, d_length, type_var, endian)

    assert result_list == result_true


convert_list_to_value_para = [
    ([1548], "int", "big", 1548),
    ([3, 232], "int", "big", 1000),
    ([171, 205], "int", "big", -21555),
    ([171, 205], "uint", "big", 43981),
    ([37775, 17072], "float", "little", 88.28820037841797),
    ([0, 0], "float", "little", 0),
]


@pytest.mark.skip(reason="To avoid accessing the server")
@pytest.mark.parametrize(
    "response, type_var, endian, result_true", convert_list_to_value_para
)
def test_convert_list_to_value(response, type_var, endian, result_true):
    result = modbus.convert_list_to_value(response, type_var, endian)

    assert result == result_true
