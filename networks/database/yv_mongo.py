#! /usr/bin/python
# coding: UTF-8

import subprocess
import pymongo
from pymongo import MongoClient, ASCENDING
import pandas as pd
from pymongo.results import InsertManyResult
from datetime import datetime, timedelta
from pytz import timezone
from typing import Any, Dict, List, Tuple


class YvMongo:
    def __init__(self, confdata: Dict[str, Any]) -> None:
        self.user_name = confdata['user_name']
        self.password = confdata['password']
        self.host_name = confdata["host_name"]
        self.port = confdata['port']
        self.db_name = confdata['db_name']
        self.col_name = confdata['collection_name']
        self.uri = f"mongodb://{self.user_name}:{self.password}@{self.host_name}:{self.port}/?replicaSet=rs0&readPreference=secondaryPreferred&retryWrites=false"

    def is_connectable(self, host: str):
        """pingによる疎通確認

        Args:
            host (str): IPアドレス

        Returns:
            bool: 疎通確認できればTrue、できなければFalse
        """
        ping = subprocess.Popen(
            ["ping", "-w", "1", "-c", "1", host], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        ping.communicate()
        return ping.returncode == 0

    def choice_hostname(self, global_ip: str, private_ip: str) -> str:
        """データベースのIPアドレスを選びます

        Args:
            global_ip (str): グローバルIP
            private_ip (str): プライベートIP

        Returns:
            str: データベースのIPアドレス
        """
        if self.is_connectable(private_ip):
            return private_ip
        else:
            return global_ip

    def show_collections(self) -> List:
        """データベース内のコレクションのリストを出力

        Returns:
            List: コレクションのリスト
        """
        with MongoClient(self.uri) as client:
            return client[self.db_name].list_collection_names()

    def count(self, doc: Dict[str, Any] = {}) -> int:
        """データ数のカウント

        Args:
            document (Dict[str, Any], optional): データの抽出条件. デフォルト値 {}.

        Returns:
            int: データのカウント数
        """
        with MongoClient(self.uri) as client:
            return client[self.db_name][self.col_name].find(filter=doc).count()

    def get_mongodata(self, start_q: str, end_q: str, fields: List[str] = [], limits: int = 0) -> List[pymongo.cursor.Cursor]:
        """mongoDBからデータを取得

        Args:
            start_q (str): 開始時刻
            end_q (str): 終了時刻
            fields (str): フィールド名

        Returns:
            List[pymongo.cursor.Cursor]: mongoDBから取得したデータ
        """
        # time
        time_dict = {'time': {'$gte': start_q, '$lt': end_q}}

        # fields
        fields_dict = {"_id": 0}
        fields_dict.update({field: 1 for field in fields})

        # mongoDBに接続
        with MongoClient(self.uri) as client:
            # データの取得
            d_dict = client[self.db_name][self.col_name].find(
                filter=time_dict,
                projection=fields_dict,
                limit=limits)

        return list(d_dict)

    def reshape_data(self, d_list: List[dict], cols_numbers: List[str] = []) -> pd.DataFrame:
        """MongoDBから取得したデータをデータフレームに変換

        Args:
            d_list (List[dict]): MongoDBから取得したデータ
            cols_numbers (List[str]): strからfloatに変換する列名

        Returns:
            pd.DataFrame: timeをDatetimeに変換したデータフレーム
        """
        # 辞書型からデータフレームに変換
        df = pd.DataFrame.from_dict(d_list).astype(object)
        # タイムゾーンを世界標準から日本時間に変換
        df['time'] = pd.DatetimeIndex(df['time']).tz_localize(
            "GMT").tz_convert('Asia/Tokyo')

        for colname in df.columns:
            if colname in cols_numbers:
                df[colname] = df[colname].str.replace(
                    ',', '').astype("float64")

        return df

    def create_index(self, fields: List, unique: bool = False) -> None:
        """コレクションにindexを貼る

        Args:
            fields (List): indexを貼るfield
            unique (bool, optional): field内の要素がuniqueかどうか. デフォルト値 False.
        """
        # mongoDBに接続
        with MongoClient(self.uri) as client:
            client[self.db_name][self.col_name].create_index(
                keys=fields, unique=unique)

    def delete(self, start_q: datetime, end_q: datetime) -> None:
        """mongoDBからデータを削除

        Args:
            start_q (datetime): 開始時刻
            end_q (datetime): 終了時刻
            fields (str): フィールド名
        """
        # mongoDBに接続
        with MongoClient(self.uri) as client:
            client[self.db_name][self.col_name].delete_many(
                filter={'time': {'$gte': start_q, '$lt': end_q}}
            )

    def insert_mongodata(self, data: Any) -> InsertManyResult:
        """mongoDBにデータを挿入

        Args:
            data (Any): 書き込むデータ

        Returns:
            InsertManyResult: mongoDBからのレスポンス
        """
        if type(data) == pd.DataFrame:
            d_dict = data.to_dict('records')
        else:
            d_dict = data

        # mongoDBに接続
        with MongoClient(self.uri) as client:
            client[self.db_name][self.col_name].insert_many(d_dict)

    def write(
        self,
        data: Any,
        index_fields: List[str],
        overwrite: bool = False
    ):
        """mongoDBにデータを書き込む

        Args:
            data (Any): 書き込むデータ
            index_fields (list): コレクションに貼るインデックス
            overwrite (bool, optional): 上書き保存のするか, しないか. デフォルト値 False (しない).
        """
        start_q, end_q = self.time_span_from_data(data)

        if self.col_name in self.show_collections():
            # データベース内に同じコレクションがあれば当日のデータを削除
            if overwrite:
                lst = self.get_mongodata(start_q, end_q + timedelta(1))
                if len(lst) > 0:
                    self.delete(start_q, end_q + timedelta(1))
        else:
            # データベース内に同じコレクションがなければ新規で作成
            self.create_index(fields=[(f, ASCENDING)
                              for f in index_fields], unique=True)

        # mongoDBに保存
        self.insert_mongodata(data)

    def time_span_from_data(self, data: Any) -> Tuple[datetime, datetime]:
        """データの開始時間と終了時間を出力

        Args:
            data (Any): データ

        Returns:
            [datetime, datetime]: 開始時間, 終了時間
        """
        if type(data) == pd.DataFrame:
            df = data
        else:
            df = pd.DataFrame(data)

        # TimeStamp -> str
        start_time = df.iloc[0]["time"].strftime("%Y-%m-%d %H:%M:%S")
        end_time = df.iloc[0]["time"].strftime("%Y-%m-%d %H:%M:%S")
        # str(JST) -> datetime(GMT)
        start_q = datetime.strptime(
            start_time, '%Y-%m-%d %H:%M:%S').astimezone(timezone('GMT'))
        end_q = datetime.strptime(
            end_time, '%Y-%m-%d %H:%M:%S').astimezone(timezone('GMT'))

        return start_q, end_q
