#! /usr/bin/python
# coding: UTF-8

import os
import re
import time
import yaml
import logging
import struct
import binascii
from typing import Any, List, Dict, Union
from pyModbusTCP.client import ModbusClient

# Set the logger
logger = logging.getLogger(__name__)
logging.getLogger("ModbusClient").setLevel(level=logging.ERROR)


class YvModbus:
    """An easy to use API based on the pyModbusTCP package"""

    def __init__(self, confdata):
        self.host = confdata["host"]
        self.port = confdata["port"]
        self.unit_id = confdata["unit_id"]

    def send(
        self,
        function_code: str = "0x01",
        addr: int = 0,
        d_length: int = 1,
        data: List[int] = [],
    ) -> List[int] | None:
        """Send requests to and receive responses from the ModBus master

        Args:
            function_code (str, optional): Function Code 0x01, 0x03, 0x04, 0x05, 0x10, or 0x0F. Defaults to "0x01".
            addr (int, optional): Starting address. Defaults to 0.
            d_length (int, optional): Length of data to request from Modbus master. Defaults to 1.
            data (List[int], optional): Data to request from Modbus master. Defaults to [].

        Returns:
            List[int] | None: Responses from Modbus Master
        """
        c = ModbusClient()
        c.host(self.host)
        c.port(self.port)
        c.unit_id(self.unit_id)
        c.timeout(30)

        # Connecting to a modbus server
        if not c.is_open():
            if not c.open():
                logging.error(
                    "unable to connect to " + self.host + ":" + str(self.port)
                )
        logging.info("connect to " + self.host + ":" + str(self.port))

        # Request and response to modbus server
        logging.info(
            "Request/function_code:"
            + function_code
            + ",unitId:"
            + str(self.unit_id)
            + ",data:"
            + str(data)
        )
        try:
            # Read coils (function 0x01)
            if function_code == "0x01":
                response = c.read_coils(addr, d_length)
            # Read input status (function 0x02)
            elif function_code == "0x02":
                response = c.read_discrete_inputs(addr, d_length)
            # Write Single coil (function 0x05)
            elif function_code == "0x05":
                is_ok = c.write_single_coil(addr, data)
                if is_ok:
                    time.sleep(0.1)
                    response = c.read_coils(addr, len(data))
                else:
                    logging.info("unable to write a bit: " + str(data))
                    response = None
            # Read holding register (function 0x03)
            elif function_code == "0x03":
                response = c.read_holding_registers(addr, d_length)
            # Read input register (function 0x04)
            elif function_code == "0x04":
                response = c.read_input_registers(addr, d_length)
            # Write multiple registers (function 0x10)
            elif function_code == "0x10":
                is_ok = c.write_multiple_registers(addr, data)
                if is_ok:
                    time.sleep(0.1)
                    response = c.read_holding_registers(addr, len(data))
                else:
                    logging.info("unable to write a register: " + str(data))
                    response = None
            # Write multiple coils (function 0x0F)
            elif function_code == "0x0F":
                is_ok = c.write_multiple_coils(addr, data)
                if is_ok:
                    time.sleep(0.1)
                    response = c.read_coils(addr, len(data))
                else:
                    logging.info("unable to write a multiple bits: " + str(data))
                    response = None
            else:
                logging.error("function code is no exist: " + function_code)
            logging.info(
                "Response/function_code:"
                + function_code
                + ",unitId:"
                + str(self.unit_id)
                + ",data:"
                + str(response)
            )
        finally:
            c.close()
            logging.info("disconnect to " + self.host + ":" + str(self.port))

        return response

    def read(self, site_id: str, system_id: str) -> Dict[str, Any]:
        """Reading data from Modbus master

        Args:
            site_id (str): Site ID (Ex. atuma, mukawa, yuza...)
            system_id (str): System ID (Ex. msystem, ioLogik1240, ioLogik1241...)

        Returns:
            Dict[str, Any]: {parameter name: value}
        """
        # Reading Modbus parameter
        modbus_paras = self.search_conf_list(site_id, system_id, m_type="read")

        # Read data from modbus master
        output_dict = {}
        for modbus_para in modbus_paras:
            # Send request to Modbus master
            response = self.send(
                function_code=modbus_para["function_code"],
                addr=modbus_para["adress"],
                d_length=sum(modbus_para["data_length"]),
            )

            # Nested responses by data length
            response_nested = self.create_nested_list(
                data_list=response, data_length=modbus_para["data_length"]
            )

            # Converts a list of responses to values and outputs them as dictionary type
            response_dict = {
                k: self.convert_list_to_value(r, t, e)
                for k, r, t, e in zip(
                    modbus_para["name"],
                    response_nested,
                    modbus_para["type"],
                    modbus_para["endian"],
                )
            }

            output_dict.update(response_dict)

        if "" in output_dict:
            del output_dict[""]

        return output_dict

    def write(
        self, site_id: str, system_id: str, data_list: List[Union[int, float]]
    ) -> Dict[str, Any]:
        """Writing data to Modbus master

        Args:
            site_id (str): Site ID (Ex. atuma, mukawa, yuza...)
            system_id (str): System ID (Ex. msystem, ioLogik1240, ioLogik1241...)
            data (List[int]): Data to be written to the device

        Returns:
            Dict[str, Any]: {parameter name: value}
        """
        # Reading Modbus parameter
        modbus_paras = self.search_conf_list(site_id, system_id, m_type="write")

        # Read data from modbus master
        output_dict = {}
        for modbus_para in modbus_paras:
            # Nested data by data length
            data_nested = self.create_nested_list(
                data_list=data_list * len(modbus_para["data_length"]),
                data_length=modbus_para["data_length"],
            )

            # Converts values to a list
            request_list = []
            for data, d_length, type_var, endian in zip(
                data_nested,
                modbus_para["data_length"],
                modbus_para["type"],
                modbus_para["endian"],
            ):
                request_list += self.convert_value_to_list(
                    value=self.convert_to_int_rounded(
                        value=data[0], target_type=type_var
                    ),
                    d_length=d_length,
                    type_var=type_var,
                    endian=endian,
                )

            # Send request to Modbus master
            response = self.send(
                function_code=modbus_para["function_code"],
                addr=modbus_para["adress"],
                data=request_list,
            )

            # Nested responses by data length
            response_nested = self.create_nested_list(
                data_list=response, data_length=modbus_para["data_length"]
            )

            response_dict = {
                k: self.convert_list_to_value(r, t, e)
                for k, r, t, e in zip(
                    modbus_para["name"],
                    response_nested,
                    modbus_para["type"],
                    modbus_para["endian"],
                )
            }

            output_dict.update(response_dict)

        if "" in output_dict:
            del output_dict[""]

        return output_dict

    def convert_to_int_rounded(
        self, value: Union[int, float], target_type: str
    ) -> Union[int, float]:
        """
        target_typeがintの場合にvalueを丸める。その他の場合はそのまま。

        Args:
            value (int or float): 変換する数値データ
            target_type (str): 変換後のデータ型を表す文字列
        Returns:
            int or float: 変換されたvalue。
        """
        if target_type in ["int", "uint"]:
            return self.round(value)
        else:
            return value

    def round(self, value: Union[int, float], digit: int = 0) -> int:
        """数値の四捨五入

        Args:
            value (Union[int, float]): 数値
            digit (int, optional): 四捨五入する桁. Defaults to 0.

        Returns:
            int: _description_
        """
        p = 10**digit

        return int((value * p * 2 + 1) // 2 / p)

    def create_nested_list(
        self, data_list: List[Union[int, float]] | None, data_length: List[int]
    ) -> List[List[Union[int, float, None]]]:
        """Create a nested list from a list of values.

        Args:
            data_list (List[Union[int, float]] | None): A list of values to be divided into sublists.
            data_length (List[int]): A list of lengths specifying the number of elements in each sublist.

        Returns:
            List[List[Union[int, float]]]: A nested list containing sublists of values from the original list.
        """
        nested_list = []
        if data_list is not None:
            # 通常のレスポンスが返ってきた場合
            for length in data_length:
                current_block = data_list[:length]
                data_list = data_list[length:]
                nested_list.append(current_block)
        else:
            # レスポンスエラーの場合
            nested_list = [[None] * length for length in data_length]

        return nested_list

    def convert_list_to_value(
        self, response: List[int | None], type_var: str, endian: str
    ) -> Any:
        """Converting a list of responses to values

        Args:
            response (List[int | None]): Response obtained from modbus master
            type_var (str): Type of variable after conversion
            endian (str): endians. big or little.

        Returns:
            Any: Converted value. int, float or None.
        """
        if response[0] is not None:
            # 通常のレスポンスが返ってきた場合
            if type_var == "bool":
                return self.convert_bool_to_int(data_list=response)
            elif type_var == "float":
                return self.convert_list_to_float(data_list=response, endian=endian)
            elif type_var == "int":
                return self.convert_list_to_int(data_list=response, endian=endian)
            elif type_var == "uint":
                return self.convert_list_to_uint(data_list=response, endian=endian)
            elif type_var == "dint":
                return self.convert_list_to_dint(data_list=response, endian=endian)
            else:
                logger.error("No exits type of variables")
        else:
            # レスポンスエラーの場合
            return None

    def convert_bool_to_int(self, data_list: List[bool]) -> int:
        """Convert value from bool to int

        Args:
            data_list (List[bool]): Value of bool

        Returns:
            list[int]: Value converted from bool to int
        """
        return int(data_list[0])

    def convert_list_to_float(self, data_list: List[int], endian: str = "big") -> float:
        """Convert a list of integers to a 32-bit hexadecimal float value.

        Args:
            data_list (List[int]): A list of integers to be converted.
            endian (str, optional): The endianness of the converted hexadecimal float value. Can be either "big" (default) or "little".

        Returns:
            float: The corresponding 32-bit hexadecimal float value.
        """
        endians_list = self.convert_endians(data_list=data_list, endian=endian)
        hex_str = self.list_to_hex(data=endians_list, zerofill=4)

        return self.hex_to_float(hex_str)

    def hex_to_float(self, s: str) -> float:
        """Convert a hexadecimal string representation of a 32-bit float to a float.

        Args:
            s (str): A hexadecimal string representation of a 32-bit float.

        Returns:
            float: The corresponding float value.
        """
        if s.startswith("0x"):
            s = s[2:]
        s = s.replace(" ", "")

        return struct.unpack(">f", binascii.unhexlify(s))[0]

    def convert_list_to_uint(self, data_list: List[int], endian: str = "big") -> int:
        """
        Convert a list of integers to a single integer by first converting the integers to hex and then to an integer.

        Args:
            data_list (List[int]): A list of integers to be converted.

        Returns:
            int: The integer obtained by converting the input list to hex and then to an integer.
        """
        # エンディアン変換
        endians_list = self.convert_endians(data_list=data_list, endian=endian)
        # 16進数の文字列に変換
        hex_str = self.list_to_hex(data=endians_list, zerofill=len(endians_list) * 2)
        # 16進数文字列を整数に変換
        num = int(hex_str, 16)

        return num

    def convert_list_to_dint(self, data_list: List[int], endian: str = "big") -> int:
        """
        Convert a list of integers representing a signed 16-bit integer (dint16)
        back to a single signed integer.

        Args:
            data_list (List[int]): A list of integers to be converted.
            endian (str, optional): The endianness of the list. Can be either "big" (default) or "little".

        Returns:
            int: The signed integer obtained by converting the input list.
        """
        # エンディアン変換
        endians_list = self.convert_endians(data_list=data_list, endian=endian)

        # 16進数の文字列に変換
        hex_str = self.list_to_hex(
            data=endians_list[1:], zerofill=len(endians_list) * 2
        )

        # 16進数文字列を整数に変換
        num = int(hex_str, 16)

        # 符号付き16ビット整数に変換（値が0x8000以上の場合は負の数に変換）
        if num >= (1 << 15):  # 16ビットでの符号ビットの確認
            num -= 1 << 16  # 符号付き整数として補正

        return num

    def convert_list_to_int(self, data_list: List[int], endian: str = "big") -> int:
        """
        Convert a list of integers to a single integer by first converting the integers to hex and then to an integer.

        Args:
            data_list (List[int]): A list of integers to be converted.

        Returns:
            int: The integer obtained by converting the input list to hex and then to an integer.
        """
        # リスト型のデータをuintに変換
        num_uint = self.convert_list_to_uint(data_list=data_list, endian=endian)

        # uintからintに変換
        num_int = self.uint_to_int(num=num_uint, data=data_list)

        return num_int

    def uint_to_int(self, num: int, data: List[int]) -> int:
        """uintからintに変換

        Args:
            num (int): uintのデータ
            data (List[int]): uintに変換する前のデータ

        Returns:
            int: intに変換後のデータ
        """
        bit_length = (2 if len(data) == 1 else len(data)) * 16
        # 符号あり整数に変換
        if num >= 2 ** (bit_length - 1):
            num -= 2**bit_length

        return num

    def convert_endians(self, data_list: List[int], endian: str = "big") -> List[int]:
        """Converts list arrangement according to endians

        Args:
            data_list (List[int]): A list of integers to be converted.
            endian (str, optional): endian. Defaults to "big".

        Returns:
            List: Converted list
        """
        data_length_half = len(data_list) // 2
        if endian == "little":
            data_list = data_list[-data_length_half:] + data_list[:data_length_half]
        elif endian != "big":
            logger.error("Endian must be either 'big' or 'little'")

        return data_list

    def list_to_hex(self, data: List[int], zerofill: int = 2) -> str:
        """Convert a list of integers to a hexadecimal string.

        Args:
            data (List[int]): A list of integers to be converted.
            zerofill (int, optional): The number of zeros to fill in the hexadecimal. Defaults to 2.

        Returns:
            str: The corresponding hexadecimal string.
        """
        hex_list = [hex(num)[2:].zfill(zerofill) for num in data]

        return "".join([x for x in hex_list])

    def convert_value_to_list(
        self, value: Union[int, float], d_length: int, type_var: str, endian: str
    ) -> List[int]:
        """Convert a numeric value to a list of integers. The type of the numeric value is specified using the `type_var` parameter.

        Args:
            value (Union[int, float]): The numeric value to be converted.
            d_length (_type_): The length of the resulting list.
            type_var (str): The type of the numeric value. Can be either "int" or "float".
            endian (str): The endianness of the resulting list. Can be either "big" (default) or "little".

        Returns:
            List[int]: The list of integers obtained by converting the numeric value to a list of integers.
        """
        if type_var == "float":
            return self.convert_float_to_list(
                value=value, endian=endian, data_length=d_length
            )
        elif type_var == "uint":
            return self.convert_uint_to_list(
                value=self.convert_to_int_rounded(value=value, target_type="int"),
                endian=endian,
                data_length=d_length,
            )
        elif type_var == "dint":
            return self.convert_dint_to_list(
                value=self.convert_to_int_rounded(value=value, target_type="int"),
                endian=endian,
                data_length=d_length,
            )
        else:
            logger.error("No exits type of variables")

    def convert_float_to_list(
        self, value: float, endian: str = "big", data_length: int = 2
    ) -> List[int]:
        """Convert a float value to a list of int.

        Args:
            f (float): A float value to be converted.
            endian (str, optional): The endianness of the converted list. Can be either "big" (default) or "little".

        Returns:
            List[int]: The corresponding list of integers.
        """
        # Convert float to binary representation
        hex_str = hex(struct.unpack("I", struct.pack("f", value))[0])[2:].zfill(8)

        # Divide by 2 digits and convert to decimal integer
        lst = [int(hex_str[i : i + 2], 16) for i in range(0, 8, 2)]

        # Swap bytes if endian is little
        if endian == "little":
            lst[:2], lst[-2:] = lst[-2:], lst[:2]

        # If data_length is 2, combine every two elements into one
        if data_length == 4:
            return lst
        elif data_length == 2:
            # data_lenth 4 -> 2
            return [lst[i] * 256 + lst[i + 1] for i in range(0, 4, 2)]
        else:
            logger.error("d_length must be either 2 or 4")

    def convert_uint_to_list(
        self, value: int, endian: str = "big", data_length: int = 1
    ) -> List[int]:
        """Convert an integer to a list of integers by first converting the integer to hex and then to a list of integers.

        Args:
            value (int): An integer to be converted.
            endian (str, optional): The endianness of the converted list. Can be either "big" (default) or "little".
            data_length (int, optional): The length of the converted list. Defaults to 4.

        Returns:
            List[int]: The list of integers obtained by converting the input integer to hex and then to a list of integers.
        """
        if data_length == 1:
            return [value] * data_length
        else:
            hex_str = hex(value)[2:].zfill(data_length * 2)
            data = [int(hex_str[i : i + 2], 16) for i in range(0, len(hex_str), 2)]
            endians_list = self.convert_endians(data, endian=endian)

            return endians_list[:data_length]

    def convert_dint_to_list(
        self, value: int, endian: str = "big", data_length: int = 1
    ) -> List[int]:
        """Convert a signed 16-bit integer (dint16) to a list of integers.

        Args:
            value (int): A signed integer to be converted.
            endian (str, optional): The endianness of the converted list. Can be either "big" (default) or "little".
            data_length (int, optional): The length of the converted list. Defaults to 1.

        Returns:
            List[int]: The list of integers obtained by converting the input signed integer.
        """
        # Handle negative values using two's complement for 16-bit signed integers
        if value < 0:
            value = (1 << 16) + value

        return [0, value]

    def search_conf_list(
        self, site_id: str, system_id: str, m_type: str = "read"
    ) -> List[Dict[str, Any]]:
        """List of parameters for reading and writing data

        Args:
            site_id (str): Site ID (Ex. atuma, mukawa, yuza...)
            system_id (str): System ID (Ex. msystem, ioLogik1240, ioLogik1241...)
            m_type (str): "read" or "write". Defaults to "read".

        Returns:
            List[Dict[str, Any]]: Function Code, address, length of data, parameter names, types, endians
        """
        # yamlからmodbusの情報を取得
        conf_path = os.path.join(os.path.dirname(__file__), f"../yml/{site_id}.yml")
        conf = self.read_conf(conf_path)
        converted_system_id = self.convert_system_id(site_id, system_id)
        modbus_para = conf["equipment"][converted_system_id][m_type]

        return modbus_para

    def read_conf(self, conf_path: str) -> Dict[str, Any]:
        """read yaml file

        Args:
            conf_path (str): path of yaml

        Returns:
            Dict[str, Any]: information of yaml
        """
        with open(conf_path, "r") as f:
            conf = yaml.safe_load(f)
        return conf

    def convert_system_id(self, site_id: str, system_id: str) -> str:
        """Convert to generic system_id

        Args:
            site_id (str): Site ID (Ex. atuma, mukawa, yuza...)
            system_id (str): Original system_id

        Returns:
            str: Converted system_id
        """
        if site_id != "kamagayama":
            if re.match(r"^pcs\d{2}$", system_id):
                system_id = "pcs"
            elif re.match(
                r"^(sm\d{2}-\d{2}|ssm\d{1}-\d{1}-\d{1}|ssm\d{1}-\d{1}-\d{1}-\d{1}|s\d{1}-\d{1}-\d{1}|s\d{3}-\d{1})$",
                system_id,
            ):
                system_id = "sm"
            else:
                system_id = system_id

        return system_id
