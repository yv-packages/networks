#! /usr/bin/python
# coding: UTF-8

import time
import threading
import traceback
import logging

from opcua import Client
from opcua import ua
from opcua.crypto import security_policies


# Set the logger
logger = logging.getLogger(__name__)
logging.getLogger("opcua").setLevel(level=logging.ERROR)


class YvOpcUa:
    """An easy to use API based on the opcua package
    """

    def __init__(self, confdata):
        self.url = f"opc.tcp://{confdata['host']}:{confdata['port']}/{confdata['server']}"
        self.policy_class = getattr(
            security_policies, 'SecurityPolicy' + confdata['security_policy'])
        self.mode = getattr(ua.MessageSecurityMode, confdata['security_mode'])
        self.certificate = confdata['certificate']
        self.private_key = confdata['private_key']
        self.items = confdata['item']

        # Connect to OPC server
        self.client = Client(self.url)
        if "username" in confdata:
            self.client.set_user(confdata["username"])
        if  "password" in confdata:
            self.client.set_password(confdata["password"])
        self.client.set_security(
            self.policy_class, self.certificate, self.private_key, None, self.mode)
        self.client.connect()

        logger.info('connect to OPC server')

    def send(self):
        """Send requests to and receive responses from the OPC Server

        Returns:
            List: Responses from OPC Server
        """
        try:
            start_date = time.time()
            # Start getting OPC data by multi-threading.
            threads = []
            for item in self.items:
                thread = opc_thread(item, self.client)
                thread.start()
                threads.append(thread)

            # Multi-threaded waiting
            for thread in threads:
                thread.join()

            logger.info('time of get opcdata : %s s', time.time() - start_date)

            # Add a response to the list
            datas = []
            for thread in threads:
                datas.append(thread.return_data)

            # Replace None with [].
            datas = [item if item is not None else [] for item in datas]
        except Exception as err:
            logger.error(traceback.format_exc())
        finally:
            self.client.disconnect()
            logger.info('disconnect to OPC server')

        return datas


# Multithreading to get opc data
class opc_thread(threading.Thread):
    def __init__(self, item, client):
        threading.Thread.__init__(self)
        self.item = item
        self.client = client
        self.return_data = None

    def run(self):
        node = self.client.get_node(self.item)
        Value = node.get_value()
        self.return_data = [Value]

    def get_value(self):
        return self.return_data
