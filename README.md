# networks

ネットワーク機器やデータベースと接続し、データの読み出しや書き込みを行うAPIをパッケージにまとめました。

# Reqirements

Python environment(python:3.12.3-slim)

# Install

```
pip install git+https://gitlab.com/yv-packages/networks.git
```

# Development

```
cd docker
docker-compose up -d --build
```

# Test

```
pytest tests/
```
